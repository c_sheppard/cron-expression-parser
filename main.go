package main

import (
	"cron-expression-parser/internal/parser"
	"flag"
	"fmt"
)

func main() {
	flag.Parse()
	cronExpression := flag.Arg(0)
	parsedCronExpression, err := parser.New().ParseCronExpression(cronExpression)
	if err != nil {
		fmt.Printf("error: %s", err.Error())
		panic(err)
	}
	fmt.Println(parsedCronExpression)
}
