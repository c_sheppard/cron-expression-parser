package cron_formatter

import (
	cron_fields "cron-expression-parser/internal/cron-fields"
	"strconv"
	"strings"
)

type CronFormatter struct {
	cronExpression CronExpression
}

func New(cronExpression CronExpression) *CronFormatter {
	return &CronFormatter{
		cronExpression: cronExpression,
	}
}

type CronExpression struct {
	CronFields map[string]*CronField
	Command    string
}

type CronField struct {
	Intervals  []int
	LowerLimit int
	UpperLimit int
	RawStr     string
}

var (
	minuteStrPrefix     = cron_fields.Minute + `        `
	hourStrPrefix       = cron_fields.Hour + `          `
	dayOfMonthStrPrefix = cron_fields.DayOfMonth + `  `
	monthStrPrefix      = cron_fields.Month + `         `
	dayOfWeekStrPrefix  = cron_fields.DayOfWeek + `   `
	commandStrPrefix    = cron_fields.Command + `       `
)

func (f *CronFormatter) FormatCronOutput(expressionStruct CronExpression) string {
	var formattedCronString string

	appendToCronString(minuteStrPrefix, expressionStruct.CronFields[cron_fields.Minute].Intervals, &formattedCronString)
	appendToCronString(hourStrPrefix, expressionStruct.CronFields[cron_fields.Hour].Intervals, &formattedCronString)
	appendToCronString(dayOfMonthStrPrefix, expressionStruct.CronFields[cron_fields.DayOfMonth].Intervals, &formattedCronString)
	appendToCronString(monthStrPrefix, expressionStruct.CronFields[cron_fields.Month].Intervals, &formattedCronString)
	appendToCronString(dayOfWeekStrPrefix, expressionStruct.CronFields[cron_fields.DayOfWeek].Intervals, &formattedCronString)
	formattedCronString += commandStrPrefix + expressionStruct.Command

	return formattedCronString
}

func appendToCronString(prefix string, cronField []int, formattedCronString *string) {
	*formattedCronString += prefix
	for i := range cronField {
		*formattedCronString += strconv.Itoa(cronField[i]) + " "
	}
	// todo: trim
	*formattedCronString = strings.TrimSpace(*formattedCronString) + "\n"
}
