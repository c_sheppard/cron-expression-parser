package expression_validator

import (
	"errors"
	"regexp"
)

var (
	InvalidCronStrError     = errors.New("invalid cron expression string")
	InvalidCronMinutesError = errors.New("invalid cron expression in minutes column")
	InvalidNumberRangeError = errors.New("invalid number range")
	TooManyFieldsError      = errors.New("too many fields in input")
	TooFewFieldsError       = errors.New("too few fields in input")
	ValidInputStringRegex   = regexp.MustCompile(`^(([/,*\-0-9])+ +){5}.*$`)
)
