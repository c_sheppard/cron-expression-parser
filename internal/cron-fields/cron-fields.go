package cron_fields

const (
	Minute     = "minute"
	Hour       = "hour"
	DayOfMonth = "day of month"
	Month      = "month"
	DayOfWeek  = "day of week"
	Command    = "command"
)
