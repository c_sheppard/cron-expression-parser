package parser

import (
	cron_fields "cron-expression-parser/internal/cron-fields"
	"cron-expression-parser/internal/cron-formatter"
	"cron-expression-parser/internal/expression-validator"
	"cron-expression-parser/internal/util"
	"fmt"
	"strings"
)

type Parser struct{}

func New() *Parser {
	return &Parser{}
}

var (
	minuteDefaultIndex     = 0
	hourDefaultIndex       = 1
	dayOfMonthDefaultIndex = 2
	monthDefaultIndex      = 3
	dayOfWeekDefaultIndex  = 4
)

var formatter cron_formatter.CronFormatter

func (p *Parser) ParseCronExpression(expressionString string) (string, error) {
	minuteCronField := cron_formatter.CronField{LowerLimit: 0, UpperLimit: 60}
	hourCronField := cron_formatter.CronField{LowerLimit: 0, UpperLimit: 24}
	dayOfMonthCronField := cron_formatter.CronField{LowerLimit: 1, UpperLimit: 32}
	monthCronField := cron_formatter.CronField{LowerLimit: 1, UpperLimit: 13}
	dayOfWeekCronField := cron_formatter.CronField{LowerLimit: 0, UpperLimit: 7}

	cronExpression := cron_formatter.CronExpression{
		CronFields: map[string]*cron_formatter.CronField{
			cron_fields.Minute:     &minuteCronField,
			cron_fields.Hour:       &hourCronField,
			cron_fields.DayOfMonth: &dayOfMonthCronField,
			cron_fields.Month:      &monthCronField,
			cron_fields.DayOfWeek:  &dayOfWeekCronField,
		},
		Command: "",
	}

	if err := parse(expressionString, &cronExpression); err != nil {
		return "", err
	}

	formattedCronExpression := formatter.FormatCronOutput(cronExpression)

	return formattedCronExpression, nil
}

func parse(expressionString string, cronExpression *cron_formatter.CronExpression) error {
	if !expression_validator.ValidInputStringRegex.MatchString(expressionString) {
		return fmt.Errorf("%w\n", expression_validator.InvalidCronStrError)
	}

	separatedFields := strings.Fields(expressionString)
	offSet := 0

	if len(separatedFields) > 6 {
		return fmt.Errorf("%w\n", expression_validator.TooManyFieldsError)
	} else if len(separatedFields) < 6 {
		return fmt.Errorf("%w\n", expression_validator.TooFewFieldsError)
	}

	(*cronExpression).CronFields[cron_fields.Minute].RawStr = separatedFields[minuteDefaultIndex+offSet]
	(*cronExpression).CronFields[cron_fields.Hour].RawStr = separatedFields[hourDefaultIndex+offSet]
	(*cronExpression).CronFields[cron_fields.DayOfMonth].RawStr = separatedFields[dayOfMonthDefaultIndex+offSet]
	(*cronExpression).CronFields[cron_fields.Month].RawStr = separatedFields[monthDefaultIndex+offSet]
	(*cronExpression).CronFields[cron_fields.DayOfWeek].RawStr = separatedFields[dayOfWeekDefaultIndex+offSet]
	cronExpression.Command = separatedFields[5]

	// todo: validate each individual expression string
	if err := getCronExpressionValues(cronExpression); err != nil {
		return err
	}

	return nil
}

func getCronExpressionValues(cronExpression *cron_formatter.CronExpression) error {
	for _, v := range cronExpression.CronFields {
		if v.RawStr == "" {
			continue
		}

		if strings.ContainsRune(v.RawStr, '-') && strings.ContainsRune(v.RawStr, ',') {
			getHyphenAndCommaIntervals(v.RawStr, &v.Intervals, v.LowerLimit, v.UpperLimit)
		} else if strings.ContainsRune(v.RawStr, '-') {
			getHyphenatedIntervals(v.RawStr, &v.Intervals, v.LowerLimit, v.UpperLimit)
		} else if strings.ContainsRune(v.RawStr, ',') {
			getCommaSeparatedIntervals(v.RawStr, &v.Intervals)
		} else if strings.Contains(v.RawStr, "*/") {
			intervalStr := strings.Trim(v.RawStr, "*/")
			interval := util.ParseInt(intervalStr)
			getAsteriskIntervals(interval, v.LowerLimit, v.UpperLimit, &v.Intervals)
		} else if v.RawStr == "*" {
			getAsteriskIntervals(1, v.LowerLimit, v.UpperLimit, &v.Intervals)
		} else {
			(*v).Intervals = append(v.Intervals, util.ParseInt(v.RawStr))
		}
		if !isValidNumbers(v.Intervals, v.LowerLimit, v.UpperLimit) {
			return fmt.Errorf("error: contains invalid values: %w",
				expression_validator.InvalidNumberRangeError)
		}
	}

	return nil
}

func getHyphenAndCommaIntervals(cronField string, cronTimings *[]int, lowerLimit, upperLimit int) {
	commaSeparatedIntervals := strings.Split(cronField, ",")

	for i := 0; i < len(commaSeparatedIntervals); i++ {
		if strings.ContainsRune(commaSeparatedIntervals[i], '-') {
			getHyphenatedIntervals(commaSeparatedIntervals[i], cronTimings, lowerLimit, upperLimit)
			continue
		}
		*cronTimings = append(*cronTimings, util.ParseInt(commaSeparatedIntervals[i]))
	}
}

func isValidNumbers(cronTimings []int, lowerLimit, upperLimit int) bool {
	for _, cronTiming := range cronTimings {
		if cronTiming < lowerLimit || cronTiming >= upperLimit {
			return false
		}
	}
	return true
}

func getCommaSeparatedIntervals(cronField string, cronTimings *[]int) {
	valuesStr := strings.Split(cronField, ",")
	for i := 0; i < len(valuesStr); i++ {
		*cronTimings = append(*cronTimings, util.ParseInt(valuesStr[i]))
	}
}

func getAsteriskIntervals(interval, lowerLimit, upperLimit int, intervals *[]int) {
	intervalRemaining := lowerLimit

	for intervalRemaining < upperLimit {
		*intervals = append(*intervals, intervalRemaining)
		intervalRemaining += interval
	}
}

func getHyphenatedIntervals(cronField string, cronTimings *[]int, lowerLimit, upperLimit int) {
	noRange := strings.Split(cronField, "-")
	first := util.ParseInt(noRange[0])
	second := util.ParseInt(noRange[1])
	if first > second {
		for i := first; i < upperLimit; i++ {
			*cronTimings = append(*cronTimings, i)
		}
		for i := lowerLimit; i <= second; i++ {
			*cronTimings = append(*cronTimings, i)
		}
	} else {
		for i := first; i <= second; i++ {
			*cronTimings = append(*cronTimings, i)
		}
	}
}
