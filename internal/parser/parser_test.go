package parser_test

import (
	"cron-expression-parser/internal/parser"
	"fmt"
	"testing"
)

func TestParser_ParseCronExpression(t *testing.T) {

	tests := []struct {
		inputExpressionString string
		expectedOutput        string
		expectedError         bool
	}{
		{
			inputExpressionString: "1-3,4,5-7,8,9 2 2 2 2 /usr/bin/find",
			expectedOutput:        `minute        1 2 3 4 5 6 7 8 9
hour          2
day of month  2
month         2
day of week   2
command       /usr/bin/find`,
			expectedError:         false,
		},
		{
			inputExpressionString: "58-2 2 2 2 2 /usr/bin/find",
			expectedOutput:        `minute        58 59 0 1 2
hour          2
day of month  2
month         2
day of week   2
command       /usr/bin/find`,
			expectedError:         false,
		},
		// day of month values out of bounds
		{
			inputExpressionString: "59 24 132 12 6 /usr/bin/find",
			expectedOutput:        ``,
			expectedError:         true,
		},
		// minutes out of bounds
		{
			inputExpressionString: "1,200 23 31 12 6 /usr/bin/find",
			expectedOutput:        ``,
			expectedError:         true,
		},
		// hour values out of bounds
		{
			inputExpressionString: "59 1,2,3,67 31 12 6 /usr/bin/find",
			expectedOutput:        ``,
			expectedError:         true,
		},
		// full range for each field
		{
			inputExpressionString: "* * * * * /usr/bin/find",
			expectedOutput: `minute        0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
hour          0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   0 1 2 3 4 5 6
command       /usr/bin/find`,
			expectedError: false,
		},
		// test */ case
		{
			inputExpressionString: "*/15 */2 */2 */2 */2 /usr/bin/find",
			expectedOutput: `minute        0 15 30 45
hour          0 2 4 6 8 10 12 14 16 18 20 22
day of month  1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31
month         1 3 5 7 9 11
day of week   0 2 4 6
command       /usr/bin/find`,
			expectedError: false,
		},
		// test all hyphenated inputs
		{
			inputExpressionString: "1-3 1-2 1-2 1-6 1-4 /usr/bin/find",
			expectedOutput: `minute        1 2 3
hour          1 2
day of month  1 2
month         1 2 3 4 5 6
day of week   1 2 3 4
command       /usr/bin/find`,
			expectedError: false,
		},
		// test all regular inputs
		{
			inputExpressionString: "15 0 15 1 5 /usr/bin/find",
			expectedOutput: `minute        15
hour          0
day of month  15
month         1
day of week   5
command       /usr/bin/find`,
			expectedError: false,
		},
		// invalid expression
		{
			inputExpressionString: "jfeurr4u8randomgarbage4lunwucia",
			expectedOutput:        ``,
			expectedError:         true,
		},
		// allow extra spaces
		{
			inputExpressionString: "*/15  0   1,15  *  1-5   /usr/bin/find",
			expectedOutput: `minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find`,
			expectedError: false,
		},
		// todo: validate against *-
		//{
		//	inputExpressionString: "*-15 0/0 1,,15 132 -1-5 /usr/bin/find",
		//	expectedOutput:        ``,
		//	expectedError:         true,
		//},
	}

	p := parser.New()

	for i := range tests {
		fmt.Printf("test %d\n", i)
		test := tests[i]
		parsedExpression, err := p.ParseCronExpression(test.inputExpressionString)

		if test.expectedError && err != nil {
			fmt.Printf("test %d: PASS - expected error: %s\n", i, err.Error())
			continue
		}

		if test.expectedError && err == nil {
			t.Errorf("test %d: expected error, got: %v", i, err)
		}

		if parsedExpression != test.expectedOutput {
			t.Errorf("test %d: error", i)
			t.Errorf("expected: \n%s", test.expectedOutput)
			t.Errorf("got: \n%s\n", parsedExpression)
		} else if !test.expectedError {
			// output the test once it has passed
			t.Logf("test %d: PASS", i)
			fmt.Printf("input: %s\n", test.inputExpressionString)
			fmt.Printf("output:\n%s\n\n", parsedExpression)
		}
	}
}
