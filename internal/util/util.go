package util

import (
	"fmt"
	"strconv"
)

func ParseInt(cronField string) int {
	intVal, err := strconv.Atoi(cronField)
	if err != nil {
		fmt.Println("something went wrong while converting string to integer value")
		panic(err)
	}
	return intVal
}
