<h1>Cron Expression Parser</h1>

<h3>Instructions to run (installing Go on OS X)</h3>

<b>Step 1:</b>
Assuming HomeBrew is installed, install Go by executing the following command in a terminal window
`brew install golang`

(from the brew installation, make sure GOPATH is set to /Users/username/go and GO111MODULE=auto)

<b>Step 2:</b>
In a terminal window, in the root directory of this repository, run:
`go run main.go "*/15 0 1,15 * 1-5 /usr/bin/find"`

The cron expression in quotes can be any valid cron expression (or an invalid expression to see the error handling).

<b>Step 3:</b>
To run the tests, run:
`go test internal/parser/parser_test.go -v`


<h3>Instructions to run (executing binary without Go installed)</h3>

<b>Step 1:</b>
In a terminal window, in the root directory of this repository, run the executable:
`./main "*/15 0 1,15 * 1-5 /usr/bin/find"`

The cron expression in quotes can be any valid cron expression (or an invalid expression to see the error handling).


<h3>Potential Improvements</h3>
- More validation on inputs
- Allow day of week strings i.e. "MON", "TUE" etc.
- Allow month strings i.e. "JAN", "FEB" etc.
- Allow special characters for day of month (e.g. '?', 'L', 'W')
- Allow special characters for day of week (e.g. '?', 'L', 'W')
- Extend for more features outside scope of exercise